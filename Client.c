 
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// PORT number
#define PORT 4444

int main()
{
	char* hello = "Hello from Client\n";
	char* chill = "Chilling Client";
	char* bye = "Client Disconnected\n";
	
	pthread_t thread_id;
	pthread_mutex_init(&lock, NULL);

	// Socket id
	int clientSocket, ret;
 
	// Client socket structure
	struct sockaddr_in cliAddr;
 
	// char array to store incoming message
	char buffer[1024];
 
	// Creating socket id
	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
 
	if (clientSocket < 0) 
	{
		printf("Error in connection.\n");
		exit(1);
	}

	printf("Client Socket is created.\n");
 
	// Initializing socket structure with NULL
	memset(&cliAddr, '\0', sizeof(cliAddr));
 
	// Initializing buffer array with NULL
	memset(buffer, '\0', sizeof(buffer));
 
	// Assigning port number and IP address
	cliAddr.sin_family = AF_INET;
	cliAddr.sin_port = htons(PORT);
 
	// 127.0.0.1 is Loopback IP
	cliAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
 
	// connect() to connect to the server
	ret = connect(clientSocket,(struct sockaddr*)&cliAddr,sizeof(cliAddr));
 
	if (ret < 0)
	{
		printf("Error in connection.\n");
		exit(1);
	}
 
	printf("Connected to Server.\n");
	char userInput[1024];
	int flag =0;
 
	while (1)
	{
 
		// recv() receives the message from server and stores in buffer
		if (recv(clientSocket, buffer, 1024, 0) < 0)
		{
			printf("Error in receiving data.\n");
			flag = 1;
			break;
		}
 
		// Printing the message on screen
		else 
		{
			printf("Server: %s\n", buffer);
			bzero(buffer, sizeof(buffer));
		}
		send(clientSocket, hello, strlen(hello), 0);

		if (pthread_create(&thread_id, NULL, &checkStatus, &clientSocket) != 0)
		{
			printf("Failed to create thread\n");
		}

		while(1)
		{
			memset(userInput, 0, sizeof(userInput));
			printf("\nPress Any or E to exit : ");
			fgets(userInput, sizeof(userInput), stdin);

			if(*userInput == 'E' || *userInput == 'e')
				{
					send(clientSocket, bye, strlen(bye), 0);
					flag = 1;
					break;
				}

			if( send(clientSocket, userInput, strlen(userInput), 0) > 0)
			{
				printf("SUCCESS");
				if(recv(clientSocket, buffer, 1024, 0) > 0)
				{
					printf(" Server: %s\n", buffer);
					memset(buffer, 0, sizeof(buffer));
				}
				else
				{
					printf("Error in receiving data!");
				}
			}
		}
		if (pthread_join(thread_id, NULL) == 0)
		{
    		printf("Failed to join thread\n");
		}

		if (flag > 0)
		{
			break;	
		}
	}
	
	close(clientSocket);
	return 0;
}